#!/bin/bash

# This script will cycle through the v4l2 devices and find the
# non-Qualcomm devices. It will print out the information that it finds.
# Note: The Qualcomm v4l2 devices are NOT UVC! Any other v4l2 devices found
#       may or may not be UVC.

device="/sys/class/video4linux/video"
devicenumber=0
devicename=$device$devicenumber/name

while [ -f $devicename ]; do
    # Simple name of the video device
    name=`cat $devicename`

    # Filter out the Qualcomm msm video devices
    if [[ $name != "msm"* ]]; then
        # Get the USB Vendor ID and Product ID from /dev/videoX/device/uevent
        usbvidpid=`cat $device$devicenumber/device/uevent | grep MODALIAS | cut -d '=' -f 2 | cut -d ':' -f 2`
        usbvid=`echo $usbvidpid | cut -c 2-5 | tr '[:upper:]' '[:lower:]'`
        usbpid=`echo $usbvidpid | cut -c 7-10 | tr '[:upper:]' '[:lower:]'`

        # Print everything out
        echo "/dev/video$devicenumber: $name $usbvid:$usbpid"
    fi

    # Only cycle through the first video0 to videoN devices. If there is a
    # jump in the numbering the loop will stop. For example, if there is video0,
    # video1, video2, and video32 then the loop will stop after video2. But that's
    # okay since the USB video devices added to the system are added at the first
    # available number. The devices after the gap are Qualcomm devices.
    ((devicenumber=devicenumber+1))
    devicename=$device$devicenumber/name
done
