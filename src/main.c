/*******************************************************************************
 * Copyright 2021 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <modal_pipe.h>

#include "libuvc/libuvc.h"
#include "config_file.h"

#define SERVER_NAME   "voxl-uvc-server"
#define DEFAULT_UVC_PIPE_NAME "uvc"
#define UVC_PIPE_TYPE "camera_image_metadata_t"

#define NUM_SUPPORTED_FRAME_FORMATS 3

#define DEBUG_PRINT(...) if(en_debug) printf(__VA_ARGS__)

static bool en_debug = false;
static bool first_frame_received = false;
static int en_config_only = 0;
static int  vendor_id = 0;
static int  product_id = 0;
static int  width = 640;
static int  height = 480;
static int  fps = 30;
static char pipe_name[MODAL_PIPE_MAX_NAME_LEN] = DEFAULT_UVC_PIPE_NAME;
static bool standalone = false;


static enum uvc_frame_format supported_frame_format[NUM_SUPPORTED_FRAME_FORMATS] =
     {UVC_FRAME_FORMAT_NV12, UVC_FRAME_FORMAT_YUYV, UVC_FRAME_FORMAT_UYVY};
static int supported_mpa_frame_format[NUM_SUPPORTED_FRAME_FORMATS] =
     {IMAGE_FORMAT_NV12, IMAGE_FORMAT_YUV422, IMAGE_FORMAT_YUV422_UYVY};
// TODO support MJPEG!
static char format_name[NUM_SUPPORTED_FRAME_FORMATS][5] = {"NV12", "YUYV", "UYVY"};
static int mpa_frame_format = -1;

// This callback function runs once per frame. Use it to perform any
// quick processing you need, or have it put the frame into your application's
// input queue. If this function takes too long, you'll start losing frames.
static void cb(uvc_frame_t *frame, void *ptr)
{

    if ( ! first_frame_received) {
        DEBUG_PRINT("Got frame callback! frame_format = %d, width = %d, height = %d, length = %lu, ptr = %p\n",
               frame->frame_format, frame->width, frame->height, frame->data_bytes, ptr);
        first_frame_received = true;
    }

    // Prepare meta data for this frame
    camera_image_metadata_t meta = {
            .magic_number = CAMERA_MAGIC_NUMBER,
            .frame_id = frame->sequence,
            .width = frame->width,
            .height = frame->height,
            .size_bytes = frame->data_bytes,
            .stride = frame->width * 2,
            .exposure_ns = -1,
            .gain = -1,
            .format = mpa_frame_format,
            .reserved = 0
        };

    // Apply proper timestamp
    struct timespec time;
    (void) clock_gettime(CLOCK_MONOTONIC, &time);
    meta.timestamp_ns = ((uint64_t) time.tv_sec) * 1000000000;
    meta.timestamp_ns += time.tv_nsec;

    // Submit metadata and frame to the pipe
    int status = pipe_server_write_camera_frame(0, meta, (const void*) frame->data);

    if (en_debug && (frame->sequence % 30 == 0)) {
        printf(" * got image %u\n",  frame->sequence);
    }
}

// If the list devices option was provided on the command line we will cycle
// through all of the devices we find and print out a bunch of diagnostic
// information. If that fails for any of the devices in the list the program
// will return an error code. The program exists after this operation.
static void list_devices()
{

    int retval = 0;
    uvc_context_t *ctx;
    uvc_device_t *dev;
    uvc_device_handle_t *devh;
    uvc_stream_ctrl_t ctrl;

    uvc_device_t **dev_list;
    uint32_t num_devices = 0;

    // Initialize a UVC service context. Libuvc will set up its own libusb
    // context. Replace NULL with a libusb_context pointer to run libuvc
    // from an existing libusb context.
    if (uvc_init(&ctx, NULL)) {
        fprintf(stderr, "Error: uvc_init failed\n");
        exit(-1);
    }

    DEBUG_PRINT("UVC initialized\n");

    printf("*** START DEVICE LIST ***\n");

    if (uvc_get_device_list(ctx, &dev_list) == UVC_SUCCESS) {
        int res;
        uvc_device_descriptor_t *desc = NULL;

        while (dev_list[num_devices] != NULL) {
            printf("\nFound device %u\n\n", num_devices);

            if ((res = uvc_get_device_descriptor(dev_list[num_devices], &desc)) != UVC_SUCCESS) {
                fprintf(stderr, "uvc_get_device_descriptor failed, Return code: ");
                uvc_perror(res, NULL);
                retval = -1;
                continue;
            }

            printf("Got device descriptor for %.4x:%.4x %s\n\n", desc->idVendor, desc->idProduct, desc->serialNumber);

            if ((res = uvc_find_device(ctx, &dev, desc->idVendor, desc->idProduct, desc->serialNumber)) != UVC_SUCCESS) {
                fprintf(stderr, "uvc_find_device failed, Return code: ");
                uvc_perror(res, NULL);
                retval = -1;
                uvc_free_device_descriptor(desc);
                desc = NULL;
                continue;
            }

            printf("Found device %.4x:%.4x\n\n", desc->idVendor, desc->idProduct);

            if ((res = uvc_open(dev, &devh)) != UVC_SUCCESS) {
                fprintf(stderr, "uvc_open failed, Return code: ");
                uvc_perror(res, NULL);
                retval = -1;
                uvc_free_device_descriptor(desc);
                desc = NULL;
                continue;
            }

            // Print out a message containing all the information that libuvc
            // knows about the device
            uvc_print_diag(devh, stdout);
            uvc_close(devh);

            num_devices++;
        }
        uvc_free_device_list(dev_list, num_devices);
    }

    if (num_devices == 0) {
        printf("\nNo devices found\n");
    }

    printf("\n*** END DEVICE LIST ***\n");

    // Close the UVC context. This closes and cleans up any existing device handles,
    // and it closes the libusb context if one was not provided.
    uvc_exit(ctx);

    DEBUG_PRINT("UVC exited\n");
    DEBUG_PRINT("voxl-uvc-server ending\n");

    exit(retval);
}

static void print_usage()
{
    // TODO add long args and bash completion
    printf("Usage: voxl-uvc-server <options>\n");
    printf("Options:\n");
    printf("-d                Show extra debug messages\n");
    printf("-v <vendor-id>    USB vendor id of the desired UVC device in hexadecimal (e.g. 090c)\n");
    printf("                  Default is search for any vendor id\n");
    printf("-p <product-id>   USB product id of the desired UVC device in hexadecimal (e.g. 337b)\n");
    printf("                  Default is search for any product id\n");
    printf("-r <resolution>   Desired image resolution (e.g. 320x240)\n");
    printf("                  Default is 640x480\n");
    printf("-f <fps>          Desired frame rate in fps (e.g. 15)\n");
    printf("                  Default is 30\n");
    printf("-l                List all devices found then exit\n");
    printf("-o <name>         Custom output name\n");
    printf("                  Default is uvc\n");
    printf("-s                Use this to launch a new instance alongside the default service\n");
    printf("-h                Show help\n");
}

static void ParseArgs(int         argc,
                      char* const argv[])
{

    // Parse all command line options
    int option;
    while ((option = getopt(argc, argv, "dcf:hlo:p:r:sv:")) != -1) {
        switch (option) {
            case 'c':
                en_config_only = 1;
			    break;

            case 'd':
                printf("Enabling debug messages\n");
                en_debug = true;
                break;

            case 'f':
                fps = (int) strtol(optarg, NULL, 10);
                break;

            case 'h':
                print_usage();
                exit(-1);

            case 'l':
                list_devices();
                break;

            case 'p':
                product_id = (int) strtol(optarg, NULL, 16);
                break;

            case 'r':
            {
                char* w = strtok(optarg, "x");
                char* h = strtok(NULL, "x");
                if(w == NULL || h == NULL) {
                    fprintf(stderr, "Error - incorrect resolution format: WIDTHxHEIGHT\n\n");
                    exit(-1);
                } else {
                    width = atoi(w);
                    height = atoi(h);
                }
                break;
            }
            case 's':
                standalone = true;
                break;
                
            case 'o':
                strncpy(pipe_name, optarg, MODAL_PIPE_MAX_NAME_LEN);
                break;

            case 'v':
                vendor_id = (int) strtol(optarg, NULL, 16);
                break;

            case '?':
                fprintf(stderr, "Error - unknown option: %c\n\n", optopt);
                print_usage();
                exit(-1);
        }
    }

    // optind is for the extra arguments which are not parsed
    for (; optind < argc; optind++) {
        fprintf(stderr, "extra arguments: %s\n", argv[optind]);
        print_usage();
        exit(-1);
    }
}

int main(int argc, char *argv[]) {

    ////////////////////////////////////////////////////////////////////////////////
    // gracefully handle an existing instance of the process and associated PID file
    ////////////////////////////////////////////////////////////////////////////////

    // make sure another instance isn't running
    // if return value is -3 then a background process is running with
    // higher privileges and we couldn't kill it, in which case we should
    // not continue or there may be hardware conflicts. If it returned -4
    // then there was an invalid argument that needs to be fixed.
    if(standalone == false && kill_existing_process(SERVER_NAME, 2.0)<-2) return -1;

    // start signal handler so we can exit cleanly
    if(enable_signal_handler()==-1){
        fprintf(stderr,"ERROR: failed to start signal handler\n");
        return -1;
    }

    // make PID file to indicate your project is running
    // due to the check made on the call to rc_kill_existing_process() above
    // we can be fairly confident there is no PID file already and we can
    // make our own safely.
    make_pid_file(SERVER_NAME);

    main_running = 1;

	////////////////////////////////////////////////////////////////////////////////
	// load config
	////////////////////////////////////////////////////////////////////////////////

	// start with the uvc config file
	printf("loading config file\n");
	if(config_file_read()) return -1;
	
    // args overwrite config file
    ParseArgs(argc, argv);

    // in config only mode, just quit now
    if(en_config_only) return 0;

    // in normal mode print the config for logging purposes
	config_file_print();

    int retval = 0;
    uvc_context_t *ctx;
    uvc_device_t *dev;
    uvc_device_handle_t *devh;
    uvc_stream_ctrl_t ctrl;

    DEBUG_PRINT("voxl-uvc-server starting\n");
    DEBUG_PRINT("Image resolution %dx%d, %d fps chosen\n", width, height, fps);
    if (vendor_id)  DEBUG_PRINT("Vendor ID 0x%.4x chosen\n", vendor_id);
    if (product_id) DEBUG_PRINT("Product ID 0x%.4x chosen\n", product_id);

    // Initialize a UVC service context. Libuvc will set up its own libusb
    // context. Replace NULL with a libusb_context pointer to run libuvc
    // from an existing libusb context.
    if (uvc_init(&ctx, NULL)) {
        fprintf(stderr, "Error: uvc_init failed\n");
        return -1;
    }

    DEBUG_PRINT("UVC initialized\n");

    // Locates the first attached UVC device, stores in dev.
    // You can filter devices by vendor_id, product_id, "serial_num"
    if (uvc_find_device(ctx, &dev, vendor_id, product_id, NULL)) {
        if ((vendor_id) || (product_id)) fprintf(stderr, "uvc_find_device failed for %.4x:%.4x\n", vendor_id, product_id);
        else fprintf(stderr, "uvc_find_device failed\n");
        retval = -1;
        goto exit;
    }

    if ((vendor_id) || (product_id)) { DEBUG_PRINT("Device %.4x:%.4x found\n", vendor_id, product_id); }
    else DEBUG_PRINT("Device found\n");

    // Try to open the device: requires exclusive access
    if (uvc_open(dev, &devh)) {
        fprintf(stderr, "uvc_open failed\n");
        retval = -1;
        goto exit;
    }

    DEBUG_PRINT("Device opened\n");

    // Negotiate desired stream configuration
    int i = 0;
    for (; i < NUM_SUPPORTED_FRAME_FORMATS; i++) {
        int res = uvc_get_stream_ctrl_format_size(
            devh, &ctrl, // result stored in ctrl
            supported_frame_format[i],
            width, height, fps
        );
        mpa_frame_format = supported_mpa_frame_format[i];
        if (res == UVC_SUCCESS) break;
    }

    if (i == NUM_SUPPORTED_FRAME_FORMATS) {
        fprintf(stderr, "uvc_get_stream_ctrl_format_size failed\n");
        fprintf(stderr, "width, height, and or framerate likely not supported\n");
        fprintf(stderr, "run voxl-uvc-server -l to list supported resolutions for your camera\n");
        retval = -1;
        goto exit;
    }

    DEBUG_PRINT("uvc_get_stream_ctrl_format_size succeeded for format %s\n", format_name[i]);

    // Print out the result
    // if (en_debug) uvc_print_stream_ctrl(&ctrl, stdout);

    // create the pipe for MPA
    pipe_info_t info = {
        .type        = UVC_PIPE_TYPE,
        .server_name = SERVER_NAME,
        .size_bytes  = (256 * MODAL_PIPE_DEFAULT_PIPE_SIZE) };

    strncpy(info.name, pipe_name, MODAL_PIPE_MAX_NAME_LEN);


    if (pipe_server_create(0, info, 0)) {
        fprintf(stderr, "pipe_server_create failed\n");
        return -1;
    }

    cJSON* json = pipe_server_get_info_json_ptr(0);
    if(json == NULL){
        fprintf(stderr, "got NULL pointer in pipe_server_get_info_json_ptr\n");
        return -1;
    }
    cJSON_AddStringToObject(json, "string_format", pipe_image_format_to_string(mpa_frame_format));
    cJSON_AddNumberToObject(json, "int_format", mpa_frame_format);
    cJSON_AddNumberToObject(json, "width", width);
    cJSON_AddNumberToObject(json, "height", height);
    cJSON_AddNumberToObject(json, "framerate", fps);
    pipe_server_update_info(0);

    // Start the video frame streaming
    if (uvc_start_streaming(devh, &ctrl, cb, NULL, 0)) {
        fprintf(stderr, "uvc_start_streaming failed\n");
        retval = -1;
        goto exit;
    }

    DEBUG_PRINT("Streaming starting\n");

    // Just sit in an endless loop until ctrl-c is pressed or we time out on
    // receiving any frames
    int frame_timeout = 0;
    while (main_running) {
        sleep(1);
        if (first_frame_received == false) {
            if (frame_timeout++ == 5) {
                fprintf(stderr, "Error, time out on receiving first frame\n");
                break;
            }
        }
    }

    // End the stream. Blocks until last callback is serviced
    uvc_stop_streaming(devh);
    DEBUG_PRINT("Done streaming\n");

    uvc_close(devh);

exit:

    pipe_server_close_all();

    // Close the UVC context. This closes and cleans up any existing device handles,
    // and it closes the libusb context if one was not provided.
    uvc_exit(ctx);

    remove_pid_file(SERVER_NAME);
    DEBUG_PRINT("UVC exited\n");
    DEBUG_PRINT("voxl-uvc-server ending\n");

    return retval;
}
